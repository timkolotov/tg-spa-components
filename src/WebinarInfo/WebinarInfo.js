import React from 'react';
import ReactMarkdown from 'react-markdown';
import PropTypes from 'prop-types';

import './WebinarInfo.scss';

const WebinarInfo = ({eventHighLight}) => (
    <div className="webinar-info d-flex flex-column align-items-center">
        <div className="webinar-info-title text-center">
            {eventHighLight.title}
        </div>
        <div className="col-lg-8 col-md-10 col-sm-12 text-center">
            <ReactMarkdown
                source={eventHighLight.contents}
                escapeHtml={false}
            />
        </div>
    </div>
);

WebinarInfo.propTypes = {
    eventHighLight: PropTypes.object.isRequired,
};

export default WebinarInfo;
