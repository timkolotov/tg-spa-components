import React from 'react';
import Img from 'gatsby-image';
import PropTypes from 'prop-types';

import './AboutCompany.scss';

const AboutCompany = ({caseStudies}) => (
    <div className="about-company">
        <div className="d-flex flex-column align-items-center">
            <div className="about-company-title text-center">
                Thorgate: The product company
            </div>
            <div className="about-company-paragraph col-lg-8 col-md-10 col-sm-12 text-center">
                <p>
                    Thorgate is a group of IT and growth companies aiming to
                    change the world with technology through access to the
                    ecosystem, digital products, and venture funding.{' '}
                </p>
                <p>
                    Our product development team supports our fields of science
                    and innovation, and also the companies we invest in. We
                    deliver quality digital products built with Python, Django,
                    React.js and React Native.
                </p>
                <p>
                    We also invest our know-how and capital in innovative
                    startups and help them succeed. Thorgate Ventures is a
                    boutique incubator with hand-picked projects and people that
                    we really want to work with.
                </p>
            </div>
        </div>
        <div className="d-flex flex-column">
            <div className="webinar-section-title text-center">
                Case studies
            </div>
            <span className="about-company-sub-header text-center">
                Some of our digitally transforming projects
            </span>
            <div className="container d-flex flex-wrap about-company-case-studies">
                <div className="d-flex flex-column col-lg-4 col-md-4 col-sm-10">
                    <Img fluid={caseStudies.ecoop.childImageSharp.fluid} />
                    <div className="case-study-title text-center">eCoop</div>
                    <a
                        href="https://thorgate.eu/ecoop/"
                        target="_blank"
                        rel="noopener noreferrer"
                        className="text-center"
                    >
                        View case study
                    </a>
                </div>
                <div className="d-flex flex-column col-lg-4 col-md-4 col-sm-10">
                    <Img fluid={caseStudies.novastar.childImageSharp.fluid} />
                    <div className="case-study-title text-center">Novastar</div>
                    <a
                        href="https://thorgate.eu/novastar/"
                        target="_blank"
                        rel="noopener noreferrer"
                        className="text-center"
                    >
                        View case study
                    </a>
                </div>
                <div className="d-flex flex-column col-lg-4 col-md-4  col-sm-10">
                    <Img fluid={caseStudies.krah.childImageSharp.fluid} />
                    <div className="case-study-title text-center">
                        Krah Pipes
                    </div>
                    <a
                        href="https://thorgate.eu/krah-pipes/"
                        target="_blank"
                        rel="noopener noreferrer"
                        className="text-center"
                    >
                        View case study
                    </a>
                </div>
            </div>
        </div>
    </div>
);

AboutCompany.propTypes = {
    caseStudies: PropTypes.shape({
        ecoop: PropTypes.object.isRequired,
        novastar: PropTypes.object.isRequired,
        krah: PropTypes.object.isRequired,
    }).isRequired,
};

export default AboutCompany;
