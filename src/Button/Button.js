import React from 'react';
import PropTypes from 'prop-types';

import './Button.scss';

const Button = ({children, ...rest}) => (
    <button className="tg-button" {...rest}>
        {children}
    </button>
);

Button.propTypes = {
    children: PropTypes.node.isRequired,
};

export default Button;
