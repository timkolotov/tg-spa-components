# Components extracted from webinar's page

**Includes**:
* Timer 
* Footer
* AboutCompany
* WebinarDateTitle
* WebinarInfo
* WebinarDetails
* WebinarFooter
* NetlifySubmissionForm
* Button
* LinkButton

## Installing
 
```bash  
yarn add https://gitlab.com/thorgate-public/tg-spa-components
```

## Examples

See how it works in `thorgate-signups` repository, `src/pages/rethinking-product-development-webinar.js` and `src/components/SubmissionForm`. 
