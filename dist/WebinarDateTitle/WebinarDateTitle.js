"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _Button = require("../Button");

require("./WebinarDateTitle.scss");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var ThorgateLogo = function ThorgateLogo(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", props, /*#__PURE__*/_react["default"].createElement("path", {
    d: "M62.995 18.67v14.586h-4.106V18.67h-4.9v-3.64h13.905v3.64h-4.9m15.494 14.586v-7.334h-5.885v7.334h-4.106V15.03h4.106v7.252h5.885V15.03h4.106v18.226h-4.106m19.872-5.309c0 2.764-1.916 5.582-7.062 5.582-5.146 0-7.062-2.818-7.062-5.582v-7.608c0-2.764 1.916-5.582 7.062-5.582s7.062 2.818 7.062 5.582v7.608zm-4.106-7.252c0-1.423-.903-2.299-2.956-2.299s-2.956.876-2.956 2.299v6.896c0 1.423.903 2.299 2.956 2.299s2.956-.876 2.956-2.299v-6.896zm12.264 6.896h-2.409v5.665h-4.106V15.03h7.582c4.435 0 6.077 1.751 6.077 5.172v2.244c0 2.272-.822 3.804-2.655 4.57l4.407 6.24h-4.872l-4.024-5.665zm3.038-7.006c0-1.368-.52-1.915-2.19-1.915h-3.257v5.364h3.257c1.752 0 2.19-.63 2.19-1.998v-1.45zm12.4 12.944c-4.434 0-6.679-2.27-6.679-5.582v-7.608c0-3.366 2.245-5.582 6.68-5.582 2.107 0 4.27.273 5.83.574l-.493 3.585c-1.588-.246-4.024-.438-5.338-.438-2.053 0-2.573 1.067-2.573 2.49v6.267c0 1.423.52 2.6 2.573 2.6.821 0 1.369-.027 2.436-.164v-5.583h3.86v8.566a23.157 23.157 0 0 1-6.296.875m23.458-.273h-4.27l-.985-3.558h-6.104l-.985 3.558h-4.27l5.556-18.226h5.474l5.584 18.226zm-10.428-6.979h4.188l-2.08-7.635-2.108 7.635zm17.163-7.607v14.586h-4.106V18.67h-4.9v-3.64h13.905v3.64h-4.9m5.346 14.586V15.03h11.468v3.64H161.6v3.338h6.789v3.64h-6.789v3.968h7.363v3.64h-11.468M23.51 47.643c12.981 0 23.505-10.521 23.505-23.5S36.491.643 23.51.643C10.528.643.005 11.164.005 24.143s10.523 23.5 23.505 23.5zm0-32.969L12.987 17.43v4.285l6.39-1.224V32.43l4.133.918 4.134-.918V20.49l6.389 1.224V17.43L23.51 14.674zm5.287 7.569v10.98l-5.287 1.524-5.287-1.524v-10.98l-3.02.61V35.66l8.307 1.83 8.307-1.83V22.852l-3.02-.61zM23.51 10.795l-8.492 2.141v2.827l8.492-2.216 8.492 2.216v-2.827l-8.492-2.141z",
    fill: "#EF6036",
    fillRule: "evenodd"
  }));
};

ThorgateLogo.defaultProps = {
  width: "169",
  height: "48",
  xmlns: "http://www.w3.org/2000/svg"
};
var weekday = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
var month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

var nth = function nth(d) {
  if (d > 3 && d < 21) return 'th';

  switch (d % 10) {
    case 1:
      return 'st';

    case 2:
      return 'nd';

    case 3:
      return 'rd';

    default:
      return 'th';
  }
};

var withZero = function withZero(time) {
  return time < 10 ? '0' + time : time;
};

var formatTime = function formatTime(date) {
  var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
  return "".concat(withZero(date.getHours() + duration), ":").concat(withZero(date.getMinutes()));
};

var WebinarDateTitle = function WebinarDateTitle(_ref) {
  var date = _ref.date,
      title = _ref.title;
  return /*#__PURE__*/_react["default"].createElement("div", {
    className: "container__webinar-date-title d-flex flex-column align-items-center"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "logo-above-title"
  }, /*#__PURE__*/_react["default"].createElement(ThorgateLogo, null)), /*#__PURE__*/_react["default"].createElement("div", {
    className: "webinar-price text-center"
  }, "Free Webinar"), /*#__PURE__*/_react["default"].createElement("div", {
    className: "webinar-title text-center pl-2 pr-2"
  }, title), /*#__PURE__*/_react["default"].createElement(_Button.LinkButton, {
    href: "#form"
  }, "Save my spot"), /*#__PURE__*/_react["default"].createElement("div", {
    className: "d-flex"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "d-flex flex-column align-items-center p-5"
  }, /*#__PURE__*/_react["default"].createElement("span", null, "Date:"), /*#__PURE__*/_react["default"].createElement("span", {
    className: "text-muted text-center"
  }, weekday[date.getDay()], ", ", date.getDate(), nth(date.getDate()), " ", month[date.getMonth()], ",", ' ', date.getFullYear())), /*#__PURE__*/_react["default"].createElement("div", {
    className: "line mt-5"
  }), /*#__PURE__*/_react["default"].createElement("div", {
    className: "d-flex flex-column align-items-center p-5"
  }, /*#__PURE__*/_react["default"].createElement("span", null, "Time:"), /*#__PURE__*/_react["default"].createElement("span", {
    className: "text-muted text-center"
  }, formatTime(date), " - ", formatTime(date, 1), " EST"))));
};

WebinarDateTitle.propTypes = {
  date: _propTypes["default"].object.isRequired,
  title: _propTypes["default"].node.isRequired
};
var _default = WebinarDateTitle;
exports["default"] = _default;