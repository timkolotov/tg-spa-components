"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "WebinarFooter", {
  enumerable: true,
  get: function get() {
    return _WebinarFooter["default"];
  }
});

var _WebinarFooter = _interopRequireDefault(require("./WebinarFooter"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }