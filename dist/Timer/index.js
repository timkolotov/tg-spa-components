"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "Timer", {
  enumerable: true,
  get: function get() {
    return _Timer["default"];
  }
});

var _Timer = _interopRequireDefault(require("./Timer"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }