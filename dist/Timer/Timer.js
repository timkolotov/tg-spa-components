"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactCountdown = _interopRequireDefault(require("react-countdown"));

require("./Timer.scss");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Completed = function Completed(_ref) {
  var children = _ref.children;
  return /*#__PURE__*/_react["default"].createElement("div", {
    className: "container"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "counter-title"
  }, children));
};

Completed.propTypes = {
  children: _propTypes["default"].node.isRequired
};

var TimeBlock = function TimeBlock(_ref2) {
  var number = _ref2.number,
      label = _ref2.label;
  return /*#__PURE__*/_react["default"].createElement("div", {
    className: "counter-block"
  }, /*#__PURE__*/_react["default"].createElement("div", null, /*#__PURE__*/_react["default"].createElement("div", {
    className: "counter-number"
  }, number), /*#__PURE__*/_react["default"].createElement("div", {
    className: "counter-label"
  }, label)));
};

TimeBlock.propTypes = {
  number: _propTypes["default"].number.isRequired,
  label: _propTypes["default"].string.isRequired
};

var Separator = function Separator() {
  return /*#__PURE__*/_react["default"].createElement("div", {
    className: "counter-separator"
  }, ":");
};

var Clock = function Clock(_ref3) {
  var days = _ref3.days,
      hours = _ref3.hours,
      minutes = _ref3.minutes,
      title = _ref3.title;
  return /*#__PURE__*/_react["default"].createElement("div", {
    className: "container"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "counter-title"
  }, /*#__PURE__*/_react["default"].createElement("h2", null, title)), /*#__PURE__*/_react["default"].createElement("div", {
    className: "counter-clock"
  }, /*#__PURE__*/_react["default"].createElement(TimeBlock, {
    number: days,
    label: "Days"
  }), /*#__PURE__*/_react["default"].createElement(Separator, null), /*#__PURE__*/_react["default"].createElement(TimeBlock, {
    number: hours,
    label: "Hours"
  }), /*#__PURE__*/_react["default"].createElement(Separator, null), /*#__PURE__*/_react["default"].createElement(TimeBlock, {
    number: minutes,
    label: "Minutes"
  })));
};

Clock.propTypes = {
  days: _propTypes["default"].number.isRequired,
  hours: _propTypes["default"].number.isRequired,
  minutes: _propTypes["default"].number.isRequired,
  title: _propTypes["default"].string.isRequired
};

var Timer = function Timer(_ref4) {
  var date = _ref4.date,
      title = _ref4.title,
      gag = _ref4.gag;

  var renderer = function renderer(_ref5) {
    var days = _ref5.days,
        hours = _ref5.hours,
        minutes = _ref5.minutes,
        completed = _ref5.completed;

    if (completed) {
      // Render a completed state
      return /*#__PURE__*/_react["default"].createElement(Completed, null, gag);
    } else {
      // Render a countdown
      return /*#__PURE__*/_react["default"].createElement(Clock, {
        days: days,
        hours: hours,
        minutes: minutes,
        title: title
      });
    }
  };

  return /*#__PURE__*/_react["default"].createElement(_reactCountdown["default"], {
    date: date,
    renderer: renderer
  });
};

Timer.propTypes = {
  date: _propTypes["default"].object.isRequired,
  title: _propTypes["default"].string.isRequired,
  gag: _propTypes["default"].node.isRequired
};
var _default = Timer;
exports["default"] = _default;