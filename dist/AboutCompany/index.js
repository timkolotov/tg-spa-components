"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "AboutCompany", {
  enumerable: true,
  get: function get() {
    return _AboutCompany["default"];
  }
});

var _AboutCompany = _interopRequireDefault(require("./AboutCompany"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }