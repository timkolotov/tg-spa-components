"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactMarkdown = _interopRequireDefault(require("react-markdown"));

var _propTypes = _interopRequireDefault(require("prop-types"));

require("./WebinarInfo.scss");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var WebinarInfo = function WebinarInfo(_ref) {
  var eventHighLight = _ref.eventHighLight;
  return /*#__PURE__*/_react["default"].createElement("div", {
    className: "webinar-info d-flex flex-column align-items-center"
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: "webinar-info-title text-center"
  }, eventHighLight.title), /*#__PURE__*/_react["default"].createElement("div", {
    className: "col-lg-8 col-md-10 col-sm-12 text-center"
  }, /*#__PURE__*/_react["default"].createElement(_reactMarkdown["default"], {
    source: eventHighLight.contents,
    escapeHtml: false
  })));
};

WebinarInfo.propTypes = {
  eventHighLight: _propTypes["default"].object.isRequired
};
var _default = WebinarInfo;
exports["default"] = _default;